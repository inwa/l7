from flask import Flask, render_template, request
import benford

app = Flask(__name__)

@app.route("/")
def hello():
    return render_template('index.html')

@app.route("/benford", methods = ['POST'])
def benford_proof():
    f = request.files['fileUpload']
    fileData = f.read()
    target_column_id = 0
    target_column_id = int(request.form.get('target_column_id'))
    delimiter = "\t"
    delimiter = request.form.get('delimiter')
    #return {'target_column_id': target_column_id, 'delimiter': delimiter, 'returnPayload': benford.proof(fileData, delimiter, target_column_id)}
    return benford.proof(fileData, delimiter, target_column_id)

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
