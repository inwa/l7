import re

def proof(userData, delimiter, target_column_id):
    lines_included = 0
    skip_heading = 1
    # shell out the distribution counts
    distribution_expected = {1:30.1, 2:17.6, 3:12.5, 4:9.7, 5:7.9, 6:6.7, 7:5.8, 8:5.1, 9:4.6}
    distribution_count = {key:0 for key in range(1,10)}
    
    for line in userData.splitlines():
        if skip_heading == 1:
            try:
                skip_heading = 0
                #headings = line.decode("utf-8").split(delimiter)
                #for col_num, heading in enumerate(headings):
                #    if heading == target_column:
                #        target_column_id = col_num
                #if (target_column_id == None):
                #    raise ValueError("Unable to locate the target column in the header row, in futility I have given up. Was the data set questionable or am I fundamentally broken?")
            except Exception as e:
                return "Exception: " + str(e)
        # process data
        else:
            decoded_line = line.decode("utf-8")
            split_line = decoded_line.split(delimiter)
            # skip lines missing the target column
            if len(split_line) <= target_column_id:
                continue
            target_value = split_line[target_column_id]
            # exclude non-numeric values
            try:
                float(target_value)
            except Exception as e:
                continue
            # guaranteed numeric data
            lines_included += 1
            try:
                if re.match("^[1-9]", target_value):
                    distribution_count[int(target_value[0])] += 1
                elif re.match("^0\.[1-9]", target_value):
                    distribution_count[int(target_value[2])] += 1
                else:
                    continue
            except:
                return 'error:' + target_value

    if lines_included > 0:
        # get ratio values as an integer with tenth places
        distribution_ratio = {key:round(distribution_count[key]/lines_included*100, 2) for key in distribution_count}
        distribution_deviation = {key:round(abs(distribution_expected[key] - distribution_ratio[key]) / distribution_expected[key] * 100, 2) for key in distribution_count}
        return {'count':distribution_count, 'ratio':distribution_ratio, 'expected':distribution_expected, 'deviation':distribution_deviation}
    else:
        return {'error': 'No numeric entries for this column'}
